package com.epam.ata.introduction;

import java.util.ArrayList;
import java.util.List;

/**
 * Exercise 4.
 * Open BrokenApplication class. Remove comments from code (you can select commented out code and do ctr + /).
  1. Find and fix compilation error.
  2. Run the BrokenApplication class (it has main method).
  3. Fix the application, so after the run it will print labels on the console.
 * <p>
 * Run and fix the application
 */
public class BrokenApplication {

    private List<String> labels;

    private List createLabels() {
        labels = new ArrayList();
        labels.add("1");
        labels.add("2");
        labels.add("3");
        labels.add("4");

        return labels;
    }

    private int getLabelsLastElementIndex() {
        return labels.size()-1;
    }

    private String getLastLabel() {
        return labels.get(getLabelsLastElementIndex());
    }

    public List<String> getLabels() {
        return labels;
    }

    public static void main(String[] args) {
        BrokenApplication application = new BrokenApplication();
//        List<String> labelList = application.getLabels();
        List<String> labelList = application.createLabels();
        labelList.add("5");
        //labelList.add(5);


        System.out.println("Print all labels: " + labelList);
        System.out.println("Print last label: " + application.getLastLabel());
    }

}
