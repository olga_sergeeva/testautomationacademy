package com.epam.ata.introduction;

/**
 * Created by Olga_Sergeeva1 on 1/18/2017.
 */
public class ArrayStatistic {
    int[] numbers;
    int theNumber;


    public ArrayStatistic(int[] input_array, int input_number) {
        numbers = input_array;
        theNumber = input_number;
    }

    public int numberOccurance() {
        int count = 0, currentInt = 0;
        for (int i = 0; i < numbers.length; i++) {
            currentInt = numbers[i];
            if (currentInt == theNumber)
                count++;
        }
        return count;
    }

    public int numberMinimum() {
        int min = numbers[0];

        for (int i = 0; i < numbers.length; i++) {
            if (min > numbers[i])
                min = numbers[i];
        }
        return min;
    }

    public int numberMaximum() {
        int max = numbers[0];

        for (int i = 0; i < numbers.length; i++) {
            if (max < numbers[i])
                max = numbers[i];
        }
        return max;
    }

    public int numberNumber() {
        int num = numbers.length;
        return num;
    }

    public int numberSum() {
        int sum = 0;

        for (int i = 0; i < numbers.length; i++) {
            sum = sum + numbers[i];
        }
        return sum;
    }

    public double numberAverage() {
        float avr = 0;

        for (int i = 0; i < numbers.length; i++) {
            avr = avr + numbers[i];
        }
        avr = avr/numbers.length;
        return avr;
    }
}