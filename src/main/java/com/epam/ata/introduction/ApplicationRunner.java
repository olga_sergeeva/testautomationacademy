package com.epam.ata.introduction;

/**

 Homework
 Open BodyMassIndex interface and look at the methods declarations and documentation.
 1. Create class which will implement this interface and will calculate BMI.
 2. Initialize the BMI class in the ApplicationRunner
 3. Setup parameters required for BMI calculation (weight/height)
 4. Initialize the ExternalSystem class in ApplicationRunner
 5. Save your BMI object using saveResults() method from external system:
 a. All required method for saving result can be found in class ExternalSystem
 b. To save the result you have to open connection, pass BMI calc object using available methods from external application
 c. Handle exceptions
 d. Make sure that you will close external application connection after saving, no matter if saving will be succesfull.
 */
public class ApplicationRunner {

    public static void main (String[] args)
    {
/*        String[] arraySTR = args[0].split(",");
        int theNumber = Integer.parseInt(args[1]);
         int[] numbers = new int[arraySTR.length];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Integer.parseInt(arraySTR[i]);
        }
*/

        ArrayStatistic arrayStatistic= new ArrayStatistic(new int[]{1,3,5,7,8,2,7,9,7}, 7);

        int count = arrayStatistic.numberOccurance();
        System.out.println("occurance = " + count);
        int min = arrayStatistic.numberMinimum();
        System.out.println("min = " + min);
        int max = arrayStatistic.numberMaximum();
        System.out.println("max = " + max);
        int num = arrayStatistic.numberNumber();
        System.out.println("num = " + num);
        int sum = arrayStatistic.numberSum();
        System.out.println("sum = " + sum);
        double avr = arrayStatistic.numberAverage();
        System.out.println("avr = " + avr);
    }
}
